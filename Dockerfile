## set base image ubuntu with generic motor
FROM registry.hzdr.de/hzb/epics/ioc/images/simmotorgenericimage AS base
## set default shell for next commands
SHELL ["/bin/bash", "-c"]

# install packages for pmac
RUN apt-get update && apt-get install git apt-utils libssh2-1 libssh2-1-dev tmux -y

# prepare environment
RUN export SUPPORT=/opt/epics/support && \
    git config --global advice.detachedHead false && \
    echo "export LC_ALL=C" >> ~/.bashrc && \
    rm /opt/epics/ioc/RELEASE.linux-x86_64.local && \
    rm -r /opt/epics/ioc/motorMotorSim

# edit file with paths
COPY configure/RELEASE.local ${SUPPORT}/RELEASE.local
COPY configure/RELEASE.local /opt/epics/ioc/RELEASE.local

# install pmac module
RUN git clone --depth 1 --recursive https://github.com/dls-controls/pmac.git ${SUPPORT}/pmac && \
    echo "BUILD_IOCS = YES" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \ 
    echo "SSH = /usr" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
    echo "SSH_LIB = \$(SSH)/lib/x86_64-linux-gnu" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
    echo "SSH_INCLUDE = -I\$(SSH)/include" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
    echo "WITH_BOOST = NO" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
    cd ${SUPPORT}/pmac/configure && \
    rm RELEASE.local RELEASE.local.linux-x86_64 RELEASE.linux-x86_64.Common && \
    cp ${SUPPORT}/RELEASE.local ${SUPPORT}/pmac/configure/RELEASE && \
    make -C ${SUPPORT}/pmac clean install 
