#include <stdio.h>
#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <stdlib.h>
#include <libssh2.h>
#include <libssh2_sftp.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>

static long mySubProcess(aSubRecord *prec) {
    // Initialize libssh2
    if (libssh2_init(0) != 0) {
        fprintf(stderr, "Error initializing libssh2.\n");
        exit(EXIT_FAILURE);
    }

    // Create a session instance
    LIBSSH2_SESSION *session;
    session = libssh2_session_init();
    if (!session) {
        fprintf(stderr, "Error creating libssh2 session.\n");
        exit(EXIT_FAILURE);
    }

    // Connect to the SSH server
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(22);
    sin.sin_addr.s_addr = inet_addr("192.168.0.200");

    if (connect(sock, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in)) != 0) {
        fprintf(stderr, "Failed to connect to SSH server.\n");
        exit(EXIT_FAILURE);
    }

    if (libssh2_session_startup(session, sock)) {
        fprintf(stderr, "Failed to establish SSH session.\n");
        exit(EXIT_FAILURE);
    }

    // Authenticate with password
    if (libssh2_userauth_password(session, "root", "deltatau")) {
        fprintf(stderr, "Authentication failed.\n");
        exit(EXIT_FAILURE);
    }

    // Send the gpascii command
    const char *gpasciiCommand = "gpascii\n";
    char buffer[2048];
    LIBSSH2_CHANNEL *channel = libssh2_channel_open_session(session);

    if (!channel) {
        fprintf(stderr, "Failed to open channel for gpascii command.\n");
        exit(EXIT_FAILURE);
    }

    if (libssh2_channel_exec(channel, gpasciiCommand)) {
        fprintf(stderr, "Failed to execute gpascii command.\n");
        exit(EXIT_FAILURE);
    }

    libssh2_channel_read(channel, buffer, sizeof(buffer) - 1);

    long *a = (long *)prec->a;   
    long *b = (long *)prec->b;

    double foundValues[*b];
    
    for(int i = *a; i <= *a+*b; i++){
        char yourCommand[50];
        sprintf(yourCommand, "P%i\n",i);
        libssh2_channel_write(channel, yourCommand, strlen(yourCommand));
        libssh2_channel_read(channel, buffer, sizeof(buffer)-1);
        char pattern[50];
        sprintf(pattern, "P%i=",i);
        char *foundStart = strstr(buffer, pattern);
        if(foundStart != NULL){
            char *foundEnd = strchr(foundStart, '\n');
            if(foundEnd != NULL){
                size_t length = foundEnd - (foundStart + strlen(pattern));
                char foundString[length+1];
                strncpy(foundString,foundStart+strlen(pattern),length); // maximum of 12 digits
                foundString[length]='\0'; 
                foundValues[i-*a]=atof(foundString);
            }
        }
    }

    memcpy(prec->vala, foundValues, sizeof(foundValues));
    
    // Close the SSH channel and free resources
    libssh2_channel_send_eof(channel);
    libssh2_channel_wait_eof(channel);
    libssh2_channel_close(channel);

    // Close the SSH session and free resources
    libssh2_session_disconnect(session, "Normal Shutdown");
    libssh2_session_free(session);
    libssh2_exit();

    // Close the socket
    close(sock);

    return 0;
}

/* Register these symbols for use by IOC code: */
epicsRegisterFunction(mySubProcess);
