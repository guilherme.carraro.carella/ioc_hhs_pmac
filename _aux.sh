
docker run -it --name epics-ubuntu registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.1.0

git clone --branch dev https://oauth2:glpat-P77GvhJbgC4-MyXhRRTs@codebase.helmholtz.cloud/guilherme.carraro.carella/ioc_hhs_pmac.git

mkdir -p /opt/epics/support
mkdir -p /opt/epics/ioc

export SUPPORT=/opt/epics/support

# configure environment

touch /opt/epics/RELEASE.local 

echo "EPICS_BASE=/opt/epics/base" >> /opt/epics/RELEASE.local &&
echo "SUPPORT=/opt/epics/support" >> /opt/epics/RELEASE.local &&
echo "ASYN=\$(SUPPORT)/asyn" >> /opt/epics/RELEASE.local &&
echo "SNCSEQ=\$(SUPPORT)/seq" >> /opt/epics/RELEASE.local &&
echo "SSCAN=\$(SUPPORT)/sscan" >> /opt/epics/RELEASE.local &&
echo "CALC=\$(SUPPORT)/calc" >> /opt/epics/RELEASE.local &&
echo "BUSY=\$(SUPPORT)/busy" >> /opt/epics/RELEASE.local &&
echo "IPAC=\$(SUPPORT)/ipac" >> /opt/epics/RELEASE.local &&
echo "MOTOR=\$(SUPPORT)/motor" >> /opt/epics/RELEASE.local &&
echo "AUTOSAVE=\$(SUPPORT)/autosave" >> /opt/epics/RELEASE.local

cp /opt/epics/RELEASE.local /opt/epics/support/RELEASE.local 

# install autosave 
git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave &&
make -C ${SUPPORT}/autosave -j $(nproc)

# install seq
git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq&&
make -C ${SUPPORT}/seq -j $(nproc)

# install sscan
git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan &&
make -C ${SUPPORT}/sscan -j $(nproc)

# install calc
git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc &&
make -C ${SUPPORT}/calc -j $(nproc)

# install asyn
git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn &&
make -C ${SUPPORT}/asyn -j $(nproc)

# install busy
git clone --depth 1 --recursive --branch R1-7-4 https://github.com/epics-modules/busy.git ${SUPPORT}/busy &&
make -C ${SUPPORT}/busy -j $(nproc)

# install ipac
git clone --depth 1 --recursive --branch 2.16 https://github.com/epics-modules/ipac.git ${SUPPORT}/ipac &&
make -C ${SUPPORT}/ipac -j $(nproc)

# install the motor support module
git clone --depth 1 --branch R7-3-1 https://github.com/epics-modules/motor ${SUPPORT}/motor &&
make -C ${SUPPORT}/motor -j $(nproc)


apt-get update
apt-get install libssh2-1 libssh2-1-dev -y

git clone --depth 1 --recursive https://github.com/dls-controls/pmac.git ${SUPPORT}/pmac


echo "BUILD_IOCS=YES" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common
echo "SSH = /usr" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common
echo "SSH_LIB         = \$(SSH)/lib/x86_64-linux-gnu" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common
echo "SSH_INCLUDE     = -I\$(SSH)/include" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common
echo "WITH_BOOST = NO" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common

rm -r ${SUPPORT}/pmac/configure/RELEASE.local.linux-x86_64 ${SUPPORT}/pmac/configure/RELEASE.linux-x86_64.Common

cp /opt/epics/RELEASE.local /opt/epics/support/pmac/configure/RELEASE.local 
cd ${SUPPORT}/pmac
make clean isntall 