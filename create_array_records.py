
# Change the value below to the number of records you want
ini_value = 8192
number_of_records = 100


with open('db/array.db', 'w') as file:
    for i in range(ini_value, ini_value+number_of_records + 1):
        record = f'record(ai, "$(P):$(Q):P{i}")' + '{' + '\n'
        record += '  field(DTYP, "asynFloat64")' + '\n'
        record += f'  field(INP, "@asyn(m1,0)PMAC_VDM_P{i}")' + '\n'
        record += f'  field(SCAN, "I/O Intr")' +  '\n'
        record += '}' + '\n\n'
        file.write(record)