from docutils import nodes
from docutils.parsers.rst import Directive

class EPICSRecordDirective(Directive):
    required_arguments = 1  # This should be the name of the db file
    has_content = False

    def run(self):
        db_name = self.arguments[0]
        try:
            lines = self.find_db_file(db_name)
            records = self.parse_records(lines)
            content = []
            for record in records:
                if record[0].startswith("####"):
                    content.extend(self.format_record_titles(record))
                elif record[0].startswith("# "):
                    content.extend(self.format_record_comment(record))
                else:
                    content.extend(self.format_record_contents(record))

            return content
        except FileNotFoundError as e:
            return [nodes.error(None, nodes.paragraph(text=str(e)))]


    def find_db_file(self, db_name):
        """
        Find the db file with the specified name, remove empty lines, and return a list with every line.
        """

        try:
            with open(db_name) as f:
                # Remove empty lines
                lines = [line for line in f.readlines() if line.strip()]
            return lines
        except FileNotFoundError as e:
            # self.app.info(str(e))
            raise FileNotFoundError(f"DB file '{db_name}' not found.")

    def parse_records(self, lines):
        """
        Parse the lines of the db file and extract EPICS records.
        """
        records = []
        current_record = []

        for line in lines:
            if line.startswith(("record","####","# ")):
                # If current_record has content, store it in records list
                if current_record:
                    records.append(current_record)
                    current_record = []
                current_record.append(line)
            else:
                current_record.append(line)
    
        if current_record:
            records.append(current_record)

        return records

    def format_record_titles(self, record_block):
        """
        Format the EPICS record titles for documentation.
        """
        formatted_titles = []

        # Iterate through lines and collect section titles and content lines
        section_title = None
        section_content = []

        for line in record_block:
            if line.startswith("####"):
                # If a new section is encountered, create a section node for the previous one
                if section_title:
                    section_node = nodes.section(ids=[nodes.make_id(section_title)])
                    section_node += nodes.title(text=section_title)
                    section_node.extend(section_content)
                    formatted_titles.append(section_node)

                # Extract the heading text without "####"
                section_title = line.strip("#").strip()
                section_content = []

        # Create a section node for the last section, if any
        if section_title:
            section_node = nodes.section(ids=[nodes.make_id(section_title)])
            section_node += nodes.title(text=section_title)
            section_node.extend(section_content)
            formatted_titles.append(section_node)

        return formatted_titles

    def format_record_comment(self, record_block):
        """
        Format the EPICS record comments as normal text for documentation.
        """
        formatted_comments = []
        for line in record_block:
            if line.startswith("# "):
                # Extract the comment text without "# "
                comment_text = line.strip("#").strip()
                # Create a paragraph node for each comment
                comment_node = nodes.paragraph(text=comment_text)
                formatted_comments.append(comment_node)
        
        return formatted_comments


    def format_record_contents(self, record_block):
        """
        Format the EPICS record contents for documentation.
        """
        # Join all lines into a single string
        record_content = ''.join(record_block)

        # Create a single literal block for the entire record
        formatted_content = [nodes.literal_block(text=record_content)]

        return formatted_content

def setup(app):
    app.add_directive("auto-epics-records", EPICSRecordDirective)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
