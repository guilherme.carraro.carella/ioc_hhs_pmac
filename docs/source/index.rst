

Documentation!
===================================

This Epics IOC was created to communicate via etherent to the PMAC Omron CK3E-1310.

Check out the :doc:`usage` section for further information.

.. note::

   This project is under active development.

Contents
--------

.. toctree:: 
   usage
   ethercat
   motor-record
   trigger

