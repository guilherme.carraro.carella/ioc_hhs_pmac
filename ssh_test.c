// compile: /usr/bin/gcc -fdiagnostics-color=always -g /opt/epics/ssh_test.c -o /opt/epics/ssh_test -lssh2^C
// run: /opt/epics/ssh_test

#include <stdio.h>
#include <stdlib.h>
#include <libssh2.h>
#include <libssh2_sftp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

void listRemoteDirectory(LIBSSH2_SESSION *session, const char *remotePath) {
    LIBSSH2_SFTP *sftpSession;
    LIBSSH2_SFTP_HANDLE *sftpHandle;

    // Initiate SFTP session
    sftpSession = libssh2_sftp_init(session);
    if (!sftpSession) {
        fprintf(stderr, "Failed to initiate SFTP session.\n");
        exit(EXIT_FAILURE);
    }

    // Open directory handle
    sftpHandle = libssh2_sftp_opendir(sftpSession, remotePath);
    if (!sftpHandle) {
        fprintf(stderr, "Failed to open directory %s\n", remotePath);
        libssh2_sftp_shutdown(sftpSession);
        exit(EXIT_FAILURE);
    }

    printf("Contents of directory %s:\n", remotePath);

    // List directory entries
    char buffer[512];
    while (libssh2_sftp_readdir(sftpHandle, buffer, sizeof(buffer), NULL) > 0) {
        printf("%s\n", buffer);
    }

    // Close directory handle
    libssh2_sftp_closedir(sftpHandle);

    // Shutdown SFTP session
    libssh2_sftp_shutdown(sftpSession);
}


int main() {

    // Initialize libssh2
    if (libssh2_init(0) != 0) {
        fprintf(stderr, "Error initializing libssh2.\n");
        exit(EXIT_FAILURE);
    }

    // Create a session instance
    LIBSSH2_SESSION *session;
    session = libssh2_session_init();
    if (!session) {
        fprintf(stderr, "Error creating libssh2 session.\n");
        exit(EXIT_FAILURE);
    }

    // Connect to the SSH server
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(22);
    sin.sin_addr.s_addr = inet_addr("192.168.0.200");

    if (connect(sock, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in)) != 0) {
        fprintf(stderr, "Failed to connect to SSH server.\n");
        exit(EXIT_FAILURE);
    }

    if (libssh2_session_startup(session, sock)) {
        fprintf(stderr, "Failed to establish SSH session.\n");
        exit(EXIT_FAILURE);
    }

    // Authenticate with password
    if (libssh2_userauth_password(session, "root", "deltatau")) {
        fprintf(stderr, "Authentication failed.\n");
        exit(EXIT_FAILURE);
    }

    // Now you can perform your SSH operations
    
    // Specify the remote folder you want to list
    const char *remoteFolder = "/var/ftp/usrflash/Project/C Language/Background Programs";

    // List contents of the remote folder
    listRemoteDirectory(session, remoteFolder);

    // Disconnect and free resources
    libssh2_session_disconnect(session, "Normal Shutdown");
    libssh2_session_free(session);
    libssh2_exit();

    // Close the socket
    close(sock);

    return 0;
}